Clea OS on RaspberryPi platforms
==============================
The manifest file at this location contains the reference to the layers 
necessary to build SECO Clea OS images for RaspberryPi boards:

* ```meta-seco-rpi```:
  [clea-os/layers/meta-seco-rpi](https://git.seco.com/clea-os/layers/seco/meta-seco-rpi.git)

    This layer contains the setup script definitions to build all RaspberryPi boards.

* ```meta-raspberrypi```:
  [https://git.yoctoproject.org/meta-raspberrypi](https://git.yoctoproject.org/meta-raspberrypi)

    This is the official layer that contains the BSP and other recipes necessary to build a Yocto image for the RaspberryPi devices.

Supported Boards
----------------------------------------

* RaspberryPi 4 in 64-bit mode


Kernel & U-Boot versions
----------------------------------------

* ```Kernel```: 5.15 ([linux-raspberrypi](https://github.com/raspberrypi/linux))
* ```U-Boot```: not directly provided by the layer (provided by poky: 2022.01)
