Clea OS on SECO MediaTek platforms
==============================
The manifest file at this location contains the reference to the layers 
necessary to build SECO Clea OS images for MediaTek CPU-based boards:

* ```meta-seco-mtk```:
  [clea-os/layers/meta-seco-mtk](https://git.seco.com/clea-os/layers/seco/meta-seco-mtk)

    This layer is intended to hold all recipes needed to build the Board Support Package (BSP) for all MediaTek CPU-based 
    SECO boards supporting Clea OS. The layer contains the reference to the actual Kernel and U-Boot
    repositories and to the commit SHAs that are used in the image build process.

* ```rity``` and ```mediatek-bsp```:
  [https://gitlab.com/mediatek/aiot/rity](https://gitlab.com/mediatek/aiot/rity)

    The Yocto layers containing MediaTek hardware support metadata.

Supported Boards
----------------------------------------
In order to check the hardware supported by the current Clea OS version, please have a look at the corresponding [Release Notes](https://git.seco.com/clea-os/seco-manifest/-/releases).

Kernel & U-Boot versions
----------------------------------------

For the Kirkstone release of Clea OS the supported Linux Kernel and U-Boot versions are:

* ```Kernel```: 5.15.x
* ```U-Boot```: 2022.10
