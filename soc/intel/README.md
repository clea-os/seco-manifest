Clea OS on SECO Intel platforms
==================================
The manifest file at this location contains the reference to the layers 
necessary to build SECO Clea OS images for Intel CPU-based boards:

* ```meta-seco-intel```:
  [clea-os/layers/meta-seco-intel](https://git.seco.com/clea-os/layers/seco/meta-seco-intel)

    This layer is intended to hold all recipes needed to build the Board Support Package (BSP) for Intel CPU-based SECO boards supporting Clea OS. 

* ```meta-intel```:
  [https://git.yoctoproject.org/meta-intel/](https://git.yoctoproject.org/meta-intel/)

    The Yocto layer containing Intel hardware support metadata.

Supported Boards
----------------------------------------
In order to check the hardware supported by the current Clea OS version, please have a look at the corresponding [Release Notes](https://git.seco.com/clea-os/seco-manifest/-/releases).
  
Kernel & U-Boot versions
----------------------------------------

For the Kirkstone release of Clea OS, the supported Linux Kernel version is:

* ```Kernel```: 5.10.x