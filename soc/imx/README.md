Clea OS on SECO NXP platforms
==============================
The manifest file at this location contains the reference to the layers 
necessary to build SECO Clea OS images for NXP CPU-based boards:

* ```meta-seco-imx```:
  [clea-os/layers/meta-seco-imx](https://git.seco.com/clea-os/layers/seco/meta-seco-imx)

    This layer is intended to hold all recipes needed to build the Board Support Package (BSP) for all NXP CPU-based 
    SECO boards supporting Clea OS. The layer contains the reference to the actual Kernel, U-Boot and mkimage
    repositories and to the commit SHAs that are used in the image build process.

* ```freescale``` and ```freescale-distro```:
  [https://github.com/Freescale](https://github.com/Freescale)

    These are mostly necessary for the i.MX8M devices mainly for VPU, GPU, and U-Boot support.

Supported Boards
----------------------------------------
In order to check the hardware supported by the current Clea OS version, please have a look at the corresponding [Release Notes](https://git.seco.com/clea-os/seco-manifest/-/releases).

Kernel & U-Boot versions
----------------------------------------

For the Kirkstone release of Clea OS the supported Linux Kernel and U-Boot versions are:

* ```Kernel```: 5.10.x
* ```U-Boot```: 202x.04