Clea OS on SECO Rockchip platforms
==================================
The manifest file at this location contains the reference to the layers 
necessary to build SECO Clea OS images for Rockchip CPU-based boards:

* ```meta-seco-rk```:
  [clea-os/layers/meta-seco-rk](https://git.seco.com/clea-os/layers/seco/meta-seco-rk)

    This layer is intended to hold all recipes needed to build the Board Support Package (BSP) for all Rockchip CPU-based 
    SECO boards supporting Clea OS. The layer contains the reference to the actual Kernel and U-Boot repositories and 
    to the corresponding commit SHAs that are used in the image build process.

Supported Boards
----------------------------------------
In order to check the hardware supported by the current Clea OS version, please have a look at the corresponding [Release Notes](https://git.seco.com/clea-os/seco-manifest/-/releases).

Kernel & U-Boot versions
----------------------------------------

For the Kirkstone release of Clea OS, the supported Linux Kernel and U-Boot versions are:

* ```Kernel```: 5.10.110 (4.19.111 temporarily supported for **C31** board only)
* ```U-Boot```: 2017.09