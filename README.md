Yocto Kirkstone manifest repository for SECO Clea OS
======================================================

This repository contains manifest files for the SECO Clea OS Board Support Package (BSP). It is the entry point for the BSP development together with the supplement tooling. See below for details.

The projects of the [Clea OS](https://git.seco.com/clea-os) GitLab group contain the source code and metadata to build Yocto images and packages for SECO devices supporting Clea OS based on the NXP i.MX6, i.MX8, Rockchip and Intel processors, and the Raspberry Pi.

The [Google `repo`](https://github.com/lipro-yocto/git-repo) tool workflow is used for the project setup.

Description
------------------------------------------------------

The build system is based on the Yocto project, further information and
documentation can be found at http://docs.yoctoproject.org/. The Yocto project
uses so called 'layers' containing the metadata to build all needed packages,
while the package sources are downloaded during the build.

The SECO Clea OS is based on the following layers:

* [poky](https://git.yoctoproject.org/git/poky.git)
* [openembedded](git://git.openembedded.org/meta-openembedded)
* [meta-clang](https://github.com/kraj/meta-clang)
* [meta-timesys](https://github.com/TimesysGit/meta-timesys)
* [meta-virtualization](https://git.yoctoproject.org/git/meta-virtualization)
* [meta-freescale](https://github.com/Freescale/meta-freescale) and [meta-freescale-distro](https://github.com/Freescale/meta-freescale-distro)
* [meta-rauc](https://github.com/rauc/meta-rauc)
* [meta-browser](https://github.com/OSSystems/meta-browser)
* [meta-rust](https://github.com/meta-rust/meta-rust)
* [meta-qt5](https://github.com/meta-qt5)
* [meta-python2](https://git.openembedded.org/meta-python2)
* [meta-intel](https://git.yoctoproject.org/meta-intel/)
* [meta-raspberrypi](https://git.yoctoproject.org/meta-raspberrypi)

SECO Clea OS comes also with specific layers internally developed:

* ```meta-seco-clea-os-things```:
  [clea-os/layers/seco/meta-seco-clea-os-things](https://git.seco.com/clea-os/layers/seco/meta-seco-clea-os-things)

    This part is intended to hold recipes that make up the SECO Clea OS Things distribution. This distro is designed to offer the highest standards in terms of security and stability thanks to features like double partitions and fallback procedures. Clea OS Things integrates a device manager to communicate 
    with the Cloud and allows a family of products to be managed as a fleet.

* ```meta-seco-clea-os-embedded```:
  [clea-os/layers/seco/meta-seco-clea-os-embedded](https://git.seco.com/clea-os/layers/seco/meta-seco-clea-os-embedded)

    This part is intended to hold recipes that make up the SECO Clea OS Embedded distribution. 
    This is the Clea OS version not supporting all IoT functionalities described above for Clea OS Things but providing a Yocto stable distro for SECO's embedded devices.

* ```meta-seco-imx```:
  [clea-os/layers/seco/meta-seco-imx](https://git.seco.com/clea-os/layers/seco/meta-seco-imx)

    This part is intended to hold all recipes needed to build the Board Support Package (BSP) for all NXP CPU-based SECO boards supporting Clea OS.

* ```meta-seco-rk```:
  [clea-os/layers/seco/meta-seco-rk](https://git.seco.com/clea-os/layers/seco/meta-seco-rk)

    This part is intended to hold all recipes needed to build the Board Support Package (BSP) for all Rockchip CPU-based SECO boards supporting Clea OS.

* ```meta-seco-intel```:
  [clea-os/layers/seco/meta-seco-intel](https://git.seco.com/clea-os/layers/seco/meta-seco-intel)

    This part is intended to hold all recipes needed to build the Board Support Package (BSP) for all Intel CPU-based SECO boards supporting Clea OS.

* ```meta-seco-rpi```:
  [clea-os/layers/seco/meta-seco-rpi](https://git.seco.com/clea-os/layers/seco/meta-seco-rpi)

    This part is intended to hold all recipes needed to build the Board Support Package (BSP) for the RaspberryPi boards.


The manifest repository
[clea-os/seco-manifest](https://git.seco.com/clea-os/seco-manifest/) contains the so called manifest
(default.xml) for the [repo tool](https://gerrit.googlesource.com/git-repo). The
manifest file contains a list of all layers and the specific revisions needed
to build the image for SECO devices, the repo tool itself reads this
manifest and handles the clone and directory setup for you. 

More specifically, SECO Clea OS manifest is splitted into several files contained in the following folders:

* ```base```:
    This folder contains the manifest files with the URL references and commit SHAs referring to the basic Yocto layers 
    (see first bulleted list in this page).

* ```distro```:
    This folder contains the manifest file with the URL references and commit SHAs referring to the 'meta-seco-clea-os-things'
    and 'meta-seco-clea-os-embedded' layers.

* ```soc```:
    This folder contains the manifest files (grouped in subfolders according to the CPU vendor) with the URL references and 
    commit SHAs referring to the 'meta-seco-imx', 'meta-seco-rk', 'meta-seco-intel' and 'meta-seco-rpi' layers.

Supported Architectures
=====================================================
Clea OS is designed to be compatible with the vast majority of the Embedded Systems produced by SECO, having both ARM and x86 architectures. Below are the currently supported architectures with links to the respective documentation for further details:

* ```NXP```: see documentation [here](soc/imx/README.md)
* ```Rockchip```: see documentation [here](soc/rk/README.md)
* ```Intel```: see documentation [here](soc/intel/README.md)
* ```Raspberry Pi```: see documentation [here](soc/rpi/README.md)

Getting started
=====================================================

Preparing the host system
------------------------------------------------------

There are some requirements to the host system. 

### Ubuntu 20.04.5

Tested distribution is currently **Ubuntu 20.04.5 LTS**.
An installation-Image can be found here:
[https://releases.ubuntu.com/20.04/ubuntu-20.04.5-desktop-amd64.iso](https://releases.ubuntu.com/20.04/ubuntu-20.04.5-desktop-amd64.iso)

This should work as virtual machine (Virtual box ...) or bare-metal installation.

#### Yocto related base software

Needed tools are listed in the 
[yocto documentation](https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html#compatible-linux-distribution).

Execute the following commands to install them:

    sudo apt install gawk wget git diffstat unzip texinfo gcc build-essential chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm python3-subunit mesa-common-dev zstd liblz4-tool

**_NOTE:_**  The packages needed to build a Yocto distribution are already installed in the dedicated Docker Image built by SECO (see below).
#### Docker build images

The release builds are executed on docker images based on [```ubuntu-20.04```](https://hub.docker.com/layers/library/ubuntu/20.04/images/sha256-bffb6799d706144f263f4b91e1226745ffb5643ea0ea89c2f709208e8d70c999?context=explore).
The SECO docker images can be found in the SECO Docker Hub [dedicated repository](https://hub.docker.com/r/secodocker/clea-os-builder) and can be pulled e.g. by doing:

    docker pull secodocker/clea-os-builder:latest

#### Development tools

Additionl development tools that may be useful:

    sudo apt install git minicom gdb-multiarch crossbuild-essential-armhf meld gedit nano cscope quilt qtcreator

#### Install the repo tool

If available the tool can also be installed from the distributions package
manager. This directly downloads the latest version from google. See https://gerrit.googlesource.com/git-repo/.

    mkdir ~/bin # once
    curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo # once
    chmod a+x ~/bin/repo # once
    echo "export PATH=${PATH}:~/bin" >> ~/.bashrc # once
    source ~/.bashrc

#### Local Git setup:

    git config --global user.name "Forename Name" # use your name instead
    git config --global user.email "my.name@example.com" # use your email instead

### Run a Docker container

Below the list of options to run a Docker container based on an image from SECO Docker Hub account (https://hub.docker.com/r/secodocker/clea-os-builder):

    export docker_user="yoctouser" ; 
    export docker_workdir="workdir" ; 

    docker run --rm -it \
      -v "${PWD}":/home/"${docker_user}"/"${docker_workdir}" \
      -v "${HOME}"/.gitconfig:/home/"${docker_user}"/.gitconfig:ro \
      -v /tmp/.X11-unix:/tmp/.X11-unix \
      -v "${HOME}"/.Xauthority:/home/"${docker_user}"/.Xauthority:rw \
      -v /etc/ssl/certs/:/etc/ssl/certs/:ro \
      --workdir=/home/"${docker_user}"/"${docker_workdir}" \
      secodocker/clea-os-builder:latest --username="${docker_user}"

**_NOTE:_**  Mount the .gitconfig file only if there are particular settings to import into the container. Otherwise, configure git settings directly inside the running docker container.

**_NOTE:_**  For internal usage, with SSH access, add the following line to the docker run command:

    -v "${HOME}"/.ssh:/home/"${docker_user}"/.ssh:ro \

**_NOTE:_** Beware to launch this command into the folder you want to work in because it will be mapped as

    /home/${docker_user}/${docker_workdir}

inside the docker container.

Download and build SECO Clea OS
------------------------------------------------------

This section describes steps to create a local build directory and how to use ```repo``` to download all needed
files. After that the build directory gets setup for one specific machine and distribution and a standard image is built.

### Download source code

    # Create a folder for the project
    mkdir -p ~/projects/clea-os
    cd ~/projects/clea-os
    
    # Initialize the manifest environment
    repo init -u https://git.seco.com/clea-os/seco-manifest.git -b kirkstone
    repo sync -j$(nproc) --fetch-submodules --no-clone-bundle

**_NOTE:_** 
    The `repo init` command above initialize a manifest based on the head on the *kirkstone* branch. In order to use a manifest based on a particular *tag*, the following command need to be used instead:

    repo init -u https://git.seco.com/clea-os/seco-manifest.git -b refs/tags/<tag>

The repo tool stores its meta data in a hidden subdirectory called ```.repo```.
The manifest repository content is stored at ```.repo/manifests/``` and is used 
for the sync command which actually downloads the sources.

The selected branch is kirkstone.

After the download the resulting directory structure should look like this:

    .
    ├── layers
    │   ├── base
    │   ├── meta-browser
    │   ├── meta-clang
    │   ├── meta-freescale
    │   ├── meta-freescale-distro
    │   ├── meta-intel
    │   ├── meta-openembedded
    │   ├── meta-python2
    │   ├── meta-qt5
    │   ├── meta-seco-clea-os-embedded
    │   ├── meta-seco-clea-os-things
    │   ├── meta-seco-imx
    │   ├── meta-seco-intel
    │   ├── meta-seco-rk
    │   ├── meta-timesys
    │   ├── meta-virtualization
    │   ├── meta-rauc
    │   └── poky
    ├── .repo
    │   ├── copy-link-files.json
    │   ├── manifests
    │   ├── manifests.git
    │   ├── manifest.xml
    │   ├── project.list
    │   ├── project-objects
    │   ├── projects
    │   ├── repo
    │   ├── TRACE_FILE
    │   └── .repo_fetchtimes.json
    └──seco-setup.sh -> layers/base/seco-setup.sh

### Build

In order to setup the build environment and run the build process, the following instructions shall be followed.  
- List the images that can be built:

    ```
    . ./seco-setup.sh -l
    ```

- Define the configuration that you need to build (e.g. 'seco_sbc_a62_clea-os' which refers to Clea OS Things image 
for A62 board):

    ```
    . ./seco-setup.sh -d seco_sbc_a62_clea-os
    ```

- Create the build environment for the selected configuration (architecture and distro features):

    ```
    . ./seco-setup.sh -c
    ```

- Finally, to build the  SECO Clea OS image:

    ```
    bitbake <image>
    ```

  Where `image` can be one of the values reported in the table below.

    | Clea OS Distro   |  image                   |   Supported   |                  Description            |
    | :--------------  | :----------------------  | :-----------  | :-------------------------------------- |
    | Things           | seco-image-clea-os       | YES           |  Base image for Clea OS Things       |
    | Embedded         | seco-image-clea-os       | NO            |                                         |
    | Embedded         | seco-image-clea-os-base  | YES           |  Minimal image for Clea OS Embedded  |
    | Embedded         | seco-image-clea-os-full  | YES           |  Full image for Clea OS Embedded     |

- For Clea OS Things only images the user can build the RAUC bundle to perform OTA updates with the command:

    ```
    bitbake seco-bundle-clea-os
    ```

Installation
---------------------------------------------------

### Installation using bmap-tools

Once the image has been built, the files are located under
```build_<board>/tmp/deploy/images/seco-<cpu>-<board>/```. The files needed to flash a complete image are the 
'bz2' compressed wic image ```seco-image-clea-os-seco-<cpu>-<board>-<build_date>.rootfs.wic.bz2``` and the bmap file 
```seco-image-clea-os-seco-<cpu>-<board>-<build_date>.rootfs.wic.bmap```. Then, in order to flash a storage device the user 
can rely on the [bmap-tools utility](https://manpages.ubuntu.com/manpages/trusty/man1/bmaptool.1.html). This is a generic 
tool for creating the block map (bmap) for a file and copying files using it. The main advantage of using bmaptool is that large files, 
like raw system image files, can be copied or flashed a lot faster with bmaptool than with traditional tools, like "dd" or "cp".

    # Install bmap-tools utility
    sudo apt-get install bmap-tools
    
    # Get the storage device name under /dev path
    ls /dev

    # Flash the image by using bmap-tools 
    sudo bmaptool copy --bmap seco-image-clea-os-seco-<cpu>-<board>-<build_date>.rootfs.wic.bmap seco-image-clea-os-seco-<cpu>-<board>-<build_date>.rootfs.wic.bz2 /dev/sda

GitLab access
---------------------------------------------------

It should not be necessary to have a SECO GitLab account to build the standard images. The public access should be sufficient to do that.

However, for customer specific layers and configurations, that are not publicly accessible, a SECO GitLab account or Access Token is necessary. Contact a SECO Software expert or the Technical Support to get access. 

You need to generate a SSH key-pair and register it with your SECO GitLab account, see the [gitlab docs](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair).

    ssh-keygen -t ed25519 -C "my.name@example.com" # use your email instead
    cat ~/.ssh/id_ed25529.pub
