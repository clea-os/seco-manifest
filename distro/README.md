Clea OS Distro for SECO boards
==================================
The manifest file at this location contains the reference to the layers 
necessary to configure SECO Clea OS distros:

* ```meta-seco-clea-os-things```:
  [clea-os/layers/meta-seco-clea-os-things](https://git.seco.com/clea-os/layers/seco/meta-seco-clea-os-things)

    This part is intended to hold recipes that make up the SECO Clea OS Things distribution. This distro is studied 
    to provide the highest standards in terms of security and stability thanks to features like delta updates, 
    double partitions, and fallback procedures. Clea OS Things integrates a device manager to communicate 
    with the Cloud and allows a family of products to be managed as a fleet.

* ```meta-seco-clea-os-embedded```:
  [clea-os/layers/meta-seco-clea-os-embedded](https://git.seco.com/clea-os/layers/seco/meta-seco-clea-os-embedded)

    This part is intended to hold recipes that make up the SECO Clea OS Embedded distribution. 
    This is the Clea OS version not supporting all IoT functionalities described above for Clea OS Things but 
    providing a Yocto stable distro for SECO's embedded devices.

* ```seco-base```:
  [clea-os/seco-base](https://git.seco.com/clea-os/seco-base)

    This repository is intended to hold all the tools necessary to configure the environment that will be used for the Yocto Image build process.
